open Ast
(* Codez ici le simplificateur de termes.

		Tout comme pour le langage du cours, l’idée consiste à remplacer les termes constants par le résultat de leur calcul.

		Faites une sous-fonctions récursive pour les exprs et les statements.
		Ces fonction font un pattern matching sur leur argument et traitent chaque cas séparément. Elles renvoient un argument de même type que celui reçu.
		Par exemple : simplify_expr : Ast.expr -> Ast.expr

		Les cas minimaux attendus sont les cas sur les entiers, les flottants, les booléens, ainsi que les if dont le test est constant, et les for qui ne s’exécutent jamais.

		Deux points qui peuvent vous permettre d’aller plus loin :
			- les exprs ne peuvent pas faire d’effet de bord ici, ce qui permet de simplifier des exprs pas nécessairement constantes.
			- Les types composés (point, position et couleur) peuvent également être simplifiés (e.g., (1,2) + (2,x) peut être simplifié en (3,2+x)).

		Vous détaillerez dans le rapport les différents cas que vous simplifiez dans votre simplificateur.
*)

let rec simplify_expr_list list = match list with
|[]->[]
|head::tail->(simplify_expr head)::(simplify_expr_list tail)

and simplify_expr expr = 
match expr with 
|Constant_i(_,_)
|Constant_f(_,_)
|Constant_b(_,_)
|Variable(_,_)->expr
|Point(e1,e2,a)-> let (ee1,ee2)=(simplify_expr e1,simplify_expr e2) in Point(ee1,ee2,a)
|Pos(e1,e2,a)->	let (ee1,ee2)=(simplify_expr e1,simplify_expr e2) in Pos(ee1,ee2,a)
|Color(e1,e2,e3,a)->let (ee1,ee2,ee3)=(simplify_expr e1,simplify_expr e2,simplify_expr e3) in Color(ee1,ee2,ee3,a)
|Unary_operator(op,e,a)->
  let ee=simplify_expr e in(
    match(op,e) with
    |(USub,Constant_i(f,_))->Constant_i(-f,a)
    |(USub,Constant_f(f,_))->Constant_f(-.f,a)
    |(Not,Constant_b(f,_))->Constant_b(not f,a)
    |(Tail,List(l,_))->(
      match l with
      |_::tail->simplify_expr (List(tail,a))
      |_->expr
    )
    |(Head,List(l,_))->(
      match l with
      |head::_->simplify_expr head
      |_->expr
    )
    |(Float_of_int,Constant_i(f,_))->Constant_f(float_of_int f,a)
    |(Floor, Constant_f(f,_))->Constant_i(int_of_float f,a)
    |(Cos,Constant_f(f,_))->Constant_f(cos f,a)
    |(Sin,Constant_f(f,_))->Constant_f(sin f,a)
    |_->Unary_operator(op,ee,a)
  )
|Binary_operator(op,e1,e2,a)->
  let (ee1,ee2)=(simplify_expr e1,simplify_expr e2) in(
    match (op,ee1,ee2) with
    |(And,Constant_b(f1,_),Constant_b(f2,_))->Constant_b(f1 && f2,a)
    |(Or,Constant_b(f1,_),Constant_b(f2,_))->Constant_b(f1 || f2,a)

    |(Add,Constant_i(f1,_),Constant_i(f2,_))->Constant_i(f1+f2,a)
    |(Add,Constant_f(f1,_),Constant_i(f2,_))->Constant_f(f1+.float_of_int f2,a)
    |(Add,Constant_i(f1,_),Constant_f(f2,_))->Constant_f(float_of_int f1 +.f2,a)
    |(Add,Constant_f(f1,_),Constant_f(f2,_))->Constant_f(f1+.f2,a)

    |(Mul,Constant_i(f1,_),Constant_i(f2,_))->Constant_i(f1*f2,a)
    |(Mul,Constant_f(f1,_),Constant_i(f2,_))->Constant_f(f1*.float_of_int f2,a)
    |(Mul,Constant_i(f1,_),Constant_f(f2,_))->Constant_f(float_of_int f1 *.f2,a)
    |(Mul,Constant_f(f1,_),Constant_f(f2,_))->Constant_f(f1*.f2,a)

    |(Div,Constant_i(f1,_),Constant_i(f2,_))->Constant_i(f1/f2,a)
    |(Div,Constant_f(f1,_),Constant_i(f2,_))->Constant_f(f1/.float_of_int f2,a)
    |(Div,Constant_i(f1,_),Constant_f(f2,_))->Constant_f(float_of_int f1 /.f2,a)
    |(Div,Constant_f(f1,_),Constant_f(f2,_))->Constant_f(f1/.f2,a)

    |(Mod,Constant_i(f1,_),Constant_i(f2,_))->Constant_i(f1 mod f2,a)
    |(Eq,Constant_i(f1,_),Constant_i(f2,_))->Constant_b(f1=f2,a)
    |(Eq,Constant_f(f1,_),Constant_f(f2,_))->Constant_b(f1=f2,a)
    |(Eq,Constant_b(f1,_),Constant_b(f2,_))->Constant_b(f1=f2,a)
    |(Ne,Constant_i(f1,_),Constant_i(f2,_))->Constant_b(f1!=f2,a)
    |(Ne,Constant_f(f1,_),Constant_f(f2,_))->Constant_b(f1!=f2,a)
    |(Ne,Constant_b(f1,_),Constant_b(f2,_))->Constant_b(f1!=f2,a)
    |(Le,Constant_i(f1,_),Constant_i(f2,_))->Constant_b(f1<=f2,a)
    |(Le,Constant_f(f1,_),Constant_f(f2,_))->Constant_b(f1<=f2,a)
    |(Lt,Constant_i(f1,_),Constant_i(f2,_))->Constant_b(f1<f2,a)
    |(Lt,Constant_f(f1,_),Constant_f(f2,_))->Constant_b(f1<f2,a)
    |(Gt,Constant_i(f1,_),Constant_i(f2,_))->Constant_b(f1>f2,a)
    |(Gt,Constant_f(f1,_),Constant_f(f2,_))->Constant_b(f1>f2,a)
    |(Ge,Constant_i(f1,_),Constant_i(f2,_))->Constant_b(f1>=f2,a)
    |(Ge,Constant_f(f1,_),Constant_f(f2,_))->Constant_b(f1>=f2,a)

    |_->Binary_operator(op,ee1,ee2,a)
  )
|Cons(e1,e2,a)->
    let (ee1,ee2)=(simplify_expr e1,simplify_expr e2) in Cons(ee1,ee2,a)
|List(l,a)->List(simplify_expr_list l, a)
| _ -> expr




let rec simplify_instr instr = match instr with
|Print(e,a)->Print(simplify_expr e,a)
|Draw(e,a)->Draw(simplify_expr e,a)
|Assignment(e1,e2,a)->Assignment(simplify_expr e1,simplify_expr e2, a)
|Block(il,a)->Block(simplify_instr_list il, a)
|IfThenElse(e,i1,i2,a)->(let ee=simplify_expr e in let (ii1,ii2)=(simplify_instr i1,simplify_instr i2)
 in (
	match ee with
	|Constant_b(true,_)->ii1
	|Constant_b(false,_)->ii2
	|_->IfThenElse(ee,ii1,ii2,a)
))
|For(name,ei,et,es,body,a)->For(name,simplify_expr ei, simplify_expr et, simplify_expr es, simplify_instr body,a)
|Foreach(name,l,body,a)->Foreach(name,simplify_expr l,simplify_instr body,a)
|_->instr

and simplify_instr_list instr_list = match instr_list with
|[]->[]
|head::tail->(simplify_instr head)::(simplify_instr_list tail)


let simplifier program = match program with
|Program(al,il)->Program(al, simplify_instr il)
