open Util

(*
   Aide :

   Ce fichier est le point d’entrée de l’analyseur sémantique. La version initiale de l’analyseur appelle l’analyseur de type, puis le simplificateur, et affiche ensuite les erreurs éventuellement ajoutées lors de ces fonctions.

   Cette version devrait être suffisante pour la partie principale du compilateur. Vous pouvez cependant être amené à ajouter d’autres analyses (par exemple, pour l’extension sur les casts implicites). Dans ce cas, créez les fonctions d’analyses pertinentes (de préférence dans d’autres fichiers), puis appelez-les simplement depuis cette fonction.

   Vous pouvez simplement ajouter un autre fichier dans ce dossier. Si vous créez le fichier "toto.ml", les fonctions qui y sont définies en préfixant leur nom par Toto (par exemple, si "toto.ml" contient la fonction "foo", alors vous pouvez appeler "Toto.foo" ici).

   Aucun fichier de configuration n’est à modifier pour cela.

*)

let analyser program text =
  let report = Error_report.create_empty text in
  Type_analyser.type_analyser report program;
  let program = Simplifier.simplifier program in
  Format.printf "@[<v 0>%a@]@," Error_report.pp_errors report;
  let incorrect = not (Error_report.has_errors report) in
  (program, incorrect)




(*
let analyser program text =
  let report = Error_report.create_empty text in
  Type_analyser.type_analyser report program;
  let program = Simplifier.simplifier program in
  check_variables report program;
  Format.printf "@[<v 0>%a@]@," Error_report.pp_errors report;
  let incorrect = not (Error_report.has_errors report) in
  (program, incorrect)

(* Fonction pour vérifier l'utilisation des variables non initialisées *)
let check_variables report program =
   let vars = ref [] in
   let rec check_expr = function
     | Ast.Var (name, _) ->
         if not (List.mem name !vars) then
           vars := name :: !vars
     | Ast.Assign (name, expr, _) ->
         check_expr expr;
         vars := List.filter (fun v -> v <> name) !vars
     | Ast.Binop (_, expr1, expr2, _) ->
         check_expr expr1;
         check_expr expr2
     | Ast.Unop (_, expr, _) ->
         check_expr expr
     | Ast.Call (_, args, _) ->
         List.iter check_expr args
     | Ast.If (expr, stmt1, stmt2, _) ->
         check_expr expr;
         check_stmt stmt1;
         check_stmt stmt2
     | Ast.While (expr, stmt, _) ->
         check_expr expr;
         check_stmt stmt
     | _ -> ()
   and check_stmt = function
     | Ast.Expr expr ->
         check_expr expr
     | Ast.Block stmts ->
         List.iter check_stmt stmts
     | Ast.If (expr, stmt1, stmt2, _) ->
         check_expr expr;
         check_stmt stmt1;
         check_stmt stmt2
     | Ast.While (expr, stmt, _) ->
         check_expr expr;
         check_stmt stmt
     | Ast.FuncDef (_, _, args, body, _) ->
         vars := args @ !vars;
         List.iter check_stmt body
     | Ast.VarDef (name, Some expr, _) ->
         check_expr expr;
         vars := List.filter (fun v -> v <> name) !vars
     | Ast.Return (Some expr, _) ->
         check_expr expr
     | _ -> ()
   in
   check_stmt program;
   List.iter
     (fun name ->
       let msg = Printf.sprintf "Variable '%s' utilisee sans etre initialisee" name in
       Error_report.add_error report msg)
     (List.rev !vars)*)