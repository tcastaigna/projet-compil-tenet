open Util
open Ast


let type_analyser report program =
  let type_environment = Environment.new_environment () in
  Environment.add type_environment "x" Type_int;
  Error_report.add_warning report
    ("sample_warning", (Lexing.dummy_pos, Lexing.dummy_pos));
  match program with _ -> ()

(*
    Réalisez ici l’analyse de type d’un programme. Faites une sous-fonction récursive pour les expressions et les statements.

    L’idée est la même que dans le langage du cours : pour chaque élément, commencez par typer ses sous-éléments. Ensuite, en fonction de l’élément et du type de ses sous-éléments, déterminez son type, et placez-le dans l’annotation qui l’accompagne.
    Seules les expressions peuvent ainsi être typées.

    Les fonctions devront manipuler un environnement qui associera chaque variable à son type. Les déclarations fixent ces types et ces types sont vérifiés lors de chaque utilisation d’une variable.

    Attention, les déclarations sont locales à un bloc, vous devez donc utiliser les fonctions Environment.add_layer et Environment.remove_layer pour le gérer.

    Cette fonction doit également effectuer les rapports d’erreur et d’avertissement dans [report].

    Ces fonction font un pattern matching sur leur argument principal et traitent chaque cas séparément. Elles font uniquement des effet de bord.
    Par exemple : type_expression : type_expression : Ast.type_expr Util.Environment.t -> Util.Error_report.t -> Ast.expression -> Ast.type_expr

    Vous pouvez également effectuer ici (en même temps ou dans une fonction séparée) une analyse d’initialisation des variables (auquel cas, il faut ajouter un argument supplémentaire à ce qui est décrit ci-dessus).

    Vous préciserez ce que vous avez traité dans votre rapport.


type type_error =
  | InvalidType of Annotation.t * type_expr * type_expr
  | UnboundVariable of Annotation.t * string

exception Type_error of type_error

let rec type_of_expression env = function
  | Constant_i (_, ann) -> Annotation.set_type ann Type_int
  | Constant_f (_, ann) -> Annotation.set_type ann Type_float
  | Constant_b (_, ann) -> Annotation.set_type ann Type_bool
  | Pos (x, y, ann) ->
      let tx = type_of_expression env x in
      let ty = type_of_expression env y in
      (match (Annotation.get_type tx, Annotation.get_type ty) with
      | (Some Type_float, Some Type_float) ->
          Annotation.set_type ann Type_pos
      | _ ->
          raise (Type_error
                  (InvalidType (ann, Type_pos, Type_list Type_float))))
  | Color (r, g, b, ann) ->
      let tr = type_of_expression env r in
      let tg = type_of_expression env g in
      let tb = type_of_expression env b in
      (match (Annotation.get_type tr, Annotation.get_type tg,
              Annotation.get_type tb) with
      | (Some Type_float, Some Type_float, Some Type_float) ->
          Annotation.set_type ann Type_color
      | _ ->
          raise (Type_error
                  (InvalidType (ann, Type_color, Type_list Type_float)))) 
  | Point (p, c, ann) ->
      let tp = type_of_expression env p in
      let tc = type_of_expression env c in
      (match (Annotation.get_type tp, Annotation.get_type tc) with
      | (Some Type_pos, Some Type_color) ->
          Annotation.set_type ann Type_point
      | _ ->
          raise (Type_error
                  (InvalidType (ann, Type_point,
                                Type_list (Type_list Type_float)))))
  | Variable (name, ann) ->
      (match List.assoc_opt name env with
      | Some ty ->
          Annotation.set_type ann ty
      | None ->
          raise (Type_error (UnboundVariable (ann, name))))            

  | Binary_operator (op, e1, e2, ann) ->
      let t1 = type_of_expression env e1 in
      let t2 = type_of_expression env e2 in
      (match (op, Annotation.get_type t1, Annotation.get_type t2) with
      | (Add, Some Type_int, Some Type_int) -> Annotation.set_type ann Type_int
      | (Sub, Some Type_int, Some Type_int) -> Annotation.set_type ann Type_int
      | (Mul, Some Type_int, Some Type_int) -> Annotation.set_type ann Type_int
      | (Div, Some Type_int, Some Type_int) -> Annotation.set_type ann Type_int
      | (Mod, Some Type_int, Some Type_int) -> Annotation.set_type ann Type_int
      | (Eq, _, _) -> Annotation.set_type ann Type_bool
      | (And, Some Type_bool, Some Type_bool) -> Annotation.set_type ann Type_bool
      | (Or, Some Type_bool, Some Type_bool) -> Annotation.set_type ann Type_bool
      | (Lt, Some Type_int, Some Type_int) -> Annotation.set_type ann Type_bool
      | (Gt, Some Type_int, Some Type_int) -> Annotation.set_type ann Type_bool
      | (Le, Some Type_int, Some Type_int) -> Annotation.set_type ann Type_bool
      | (Ge, Some Type_int, Some Type_int) -> Annotation.set_type ann Type_bool
      | (op, t1, t2) ->
          (*raise (Type_error (InvalidType (ann, Annotation.get_type t1,Annotation.get_type t2))))*)
          raise (Type_error
                  (InvalidType (ann, Type_pos, Type_list Type_float)))) (* pour essayer*)
      | _ -> failwith "type error: not matched" (* pour essayer*)
    (*Unary operator case*)
  (*| Unary_operator (op, e1, ann) ->
    match op with
      | "-" -> if type_of_expression e1 = "int" then "int" else failwith "type error: unary operator '-'"
      | "not" -> if type_of_expression e1 = "bool" then "bool" else failwith "type error: unary operator 'not'"
      | _ -> failwith "unknown unary operator"

    *)
    
    (*Field accessor case*)
  (*| Field_accessor (field, e, _) -> 
      match field with
      | Color_accessor -> Color_type
      | Position_accessor -> Position_type
      | X_accessor -> Float_type
      | Y_accessor -> Float_type
      | Blue_accessor -> Float_type
      | Red_accessor -> Float_type
      | Green_accessor -> Float_type*)
  

let type_analyser report program =
  let type_environment = Environment.new_environment () in
  Environment.add type_environment "x" Type_int;
  Error_report.add_warning report
    ("sample_warning", (Lexing.dummy_pos, Lexing.dummy_pos));
  match program with
    | exp -> let _ = type_of_expression (List.of_enum @@ Environment.bindings type_environment) exp in ()




(*--------------OMITTED CODE----------------------------*)

(* let type_of_binop = function
  | Add | Sub | Mul | Div | Mod -> Type_int
  (*| Add_f | Sub_f | Mul_f | Div_f | Mod_f -> TFloat*)
  | And | Or -> Type_bool
  | Eq | Ne | Le | Ge | Lt | Gt -> Type_bool *)

(*let is_comparison_binop = function
  | Eq | Ne | Le | Ge | Lt | Gt -> true
  | _ -> false

  *)

(* let int_binop_of_binop = function
    | Add | Add_f -> Add
    | Sub | Sub_f -> Sub
    | Mul | Mul_f -> Mul
    | Div | Div_f -> Div
    | Mod | Mod_f -> Mod
    | o -> o

  let float_binop_of_binop = function
    | Add | Add_f -> Add_f
    | Sub | Sub_f -> Sub_f
    | Mul | Mul_f -> Mul_f
    | Div | Div_f -> Div_f
    | Mod | Mod_f -> Mod_f
    | o -> o

  let int_unop_of_op = function UMin_f -> UMin | o -> o
  let float_unop_of_op = function UMin -> UMin_f | o -> o


let rec types_equal t1 t2 =
  match t1, t2 with
  | Type_bool, Type_bool -> true
  | Type_int,Type_int -> true
  | Type_float,Type_float -> true
  | Type_pos, Type_pos -> true
  | Type_color, Type_color -> true
  | Type_point, Type_point -> true
  | Type_list t1', Type_list t2' -> types_equal t1' t2'
  | _, _ -> false *)
(*
(* Helper function to check if a type is numeric *)
let is_numeric = function
  | Type_int | Type_float -> true
  | _ -> false

(* Helper function to apply a binary operator to two expressions *)
let rec apply_binop op e1 e2 =
  match op, e1, e2 with
  | Add, Constant_i (i1,a1) , Constant_i (i2,_) -> Constant_i ((i1 + i2),a1) 
  | Add, Constant_f (f1,a1), Constant_f (f2,_) -> Constant_f (f1 +. f2,a1)
  | Sub, Constant_i (i1, a1), Constant_i (i2, _) -> Constant_i ((i1 - i2),a1)
  | Sub, Constant_f (f1, a1), Constant_f (f2, _) -> Constant_f ((f1 -. f2),a1)
  | Mul, Constant_i (i1, a1), Constant_i (i2, _) -> Constant_i ((i1 * i2),a1)
  | Mul, Constant_f (f1, a1), Constant_f (f2, _) -> Constant_f ((f1 *. f2),a1)
  | Div, Constant_i (i1, a1), Constant_i (i2, _) -> Constant_f ((float_of_int i1 /. float_of_int i2),a1)
  | Div, Constant_f (f1, a1), Constant_f (f2, _) -> Constant_f ((f1 /. f2), a1)
  | Mod, Constant_i (i1, a1), Constant_i (i2, _) -> Constant_i ((i1 mod i2), a1)
  | Or, Constant_b (b1, a1), Constant_b (b2, _) -> Constant_b ((b1 || b2), a1)
  | And, Constant_b (b1, a1), Constant_b (b2, _) -> Constant_b ((b1 && b2), a1)
  (*| Eq, e1, e2 -> Constant_b (types_equal (infer_type e1) (infer_type e2), (Annotation.create ()))
  | Ne, e1, e2 -> Constant_b (not (types_equal (infer_type e1) (infer_type e2)), Annotation.create ())*)
  | Eq, e1, e2 -> e1
  | Ne, e1, e2 -> e1
  | Lt, Constant_i (i1, a1), Constant_i (i2, _) -> Constant_b ((i1 < i2), a1)
  | Lt, Constant_f (f1, a1), Constant_f (f2, _) -> Constant_b ((f1 < f2), a1)
  | Le, Constant_i (i1, a1), Constant_i (i2, _) -> Constant_b ((i1 <= i2), a1)
  | Le, Constant_f (f1, a1), Constant_f (f2, _) -> Constant_b ((f1 <= f2), a1)
  | Gt, Constant_i (i1,a1), Constant_i (i2,_) -> Constant_b (i1 > i2,a1)
  | Gt, Constant_f (f1,a1), Constant_f (f2,_) -> Constant_b (f1 > f2,a1)
  | Ge, Constant_i (i1,a1), Constant_i (i2,_) -> Constant_b (i1 >= i2,a1)
  | Ge, Constant_f (f1,a1), Constant_f (f2,_) -> Constant_b (f1 >= f2,a1)
  | _, _, _ -> raise (TypeError("Invalid binary operator"))
  

  let rec type_check_expr (env:env) (expr:expr) : typ option =
    match expr with
    | Expr_bool _ -> Some Type_bool
    | Expr_int _ -> Some Type_int
    | Expr_float _ -> Some Type_float
    | Expr_pos _ -> Some Type_pos
    | Expr_color _ -> Some Type_color
    | Expr_point _ -> Some Type_point
    | Expr_ident id -> lookup id env
    | Expr_binop (op, e1, e2) ->
        let t1 = type_check_expr env e1 in
        let t2 = type_check_expr env e2 in
        (match op with
        | Op_add | Op_sub | Op_mult | Op_div ->
            (match t1, t2 with
            | Some Type_int, Some Type_int -> Some Type_int
            | Some Type_float, Some Type_float -> Some Type_float
            | _, _ -> None)
        | Op_eq | Op_lt | Op_lte | Op_gt | Op_gte ->
            (match t1, t2 with
            | Some t1', Some t2' when types_equal t1' t2' -> Some Type_bool
            | _, _ -> None))
    | Expr_if (e1, e2, e3) ->
        (match type_check_expr env e1, type_check_expr env e2, type_check_expr env e3 with
        | Some Type_bool, Some t2, Some t3 when types_equal t2 t3 -> Some t2
        | _, _, _ -> None)
    | Expr_func (arg, arg_type, body) ->
        let env' = bind arg arg_type env in
        let body_type = type_check_expr env' body in
        (match body_type with
        | Some body_type' -> Some (Type_func (arg_type, body_type'))
        | None -> None)
    | Expr_let (id, e1, e2) ->
        let t1 = type_check_expr env e1 in
        (match t1 with
        | Some t1' -> type_check_expr (bind id t1' env) e2
        | None -> None)
    | Expr_apply (e1, e2) ->
        (match type_check_expr env e1 with
        | Some (Type_func (t1, t2)) when type_check_expr env e2 = Some t1 -> Some t2
        | _ -> None)
    | Expr_list l ->
        let rec check_list l =
          match l with
          | [] -> Some (Type_list Type_int)
          | x::xs ->
              let t = type_check_expr env x in
              if t = type_check_expr env (Expr_list xs) then t else None
        in check_list l
    | Expr_emptylist -> Some (Type_list Type_int)
    *)*)