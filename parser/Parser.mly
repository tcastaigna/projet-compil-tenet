%{
    open Ast

    (*Le parseur doit générer des Ast.program.
    Pour les annotation, il vous suffit de créer l’annotation avec [Annotation.create $loc] qui sera ce qu’il vous faut dans tous les cas : $loc est un paramètre qui correspond aux positions délimitant la chaîne de caractères parsée par la règle où il est utilisé, ce qui est exactement ce qu’il faut mettre dans l’annotation.*)
%}

/* les type_basics de tokens que le parser va reconnaitre */
%token EOF
%token <int> INT
%token <float> FLOAT
%token <string> ID
%token <bool> BOOL
%token AND BEGIN BLUE BOOL_TYP COLOR COPY COS DRAW ELSE END FLOAT_TYP FLOAT_OF_INT FLOOR FOR FOREACH FROM GREEN HEAD IF IN INT_TYP LIST NOT //OP
%token OR POINT POS PRINT RED SIN STEP TAIL TO X Y PREPEND
%token OPENCR CLOSECR OPENPA CLOSEPA COMMA SEMICOLON
%token PLUS MINUS TIMES DIVIDE MODULO DOT GREY TRANSLATE
%token EQUALS NOTEQUALS LESSTHAN LESSTHANEQUALS GREATERTHAN GREATERTHANEQUALS 
%token PI 

//%nonassoc THEN
%left PREPEND
%left AND OR
%left EQUALS NOTEQUALS LESSTHAN GREATERTHAN LESSTHANEQUALS GREATERTHANEQUALS
%left PLUS MINUS
%left TIMES DIVIDE MODULO 
%left DOT
%nonassoc SIN COS
%nonassoc NOT 

%start <program> main
%%

main:
| LESSTHAN al = arg_list GREATERTHAN il = block EOF {Program(al,il)}
| block = block EOF { Program([], block) }
| EOF { Program([],Block([],Annotation.create $loc)) }



block:
| BEGIN il = instr_list END { Block(il, Annotation.create $loc) }



instr_list:
| e = instr { [e] }
| t = instr_list SEMICOLON e = instr { t@[e] }
| { [] }

expr_list:
| e = expr { [e] }
| t = expr_list COMMA e = expr { t@[e] }
| { [] }

arg_list:
| i = argument { [i] }
| t = arg_list SEMICOLON i = argument { t@[i] }
| { [] }


instr:
| PRINT e = expr { Print(e, Annotation.create $loc) }
| DRAW e = expr { Draw(e,Annotation.create $loc)}
| COPY e1 = var e2 = expr {Assignment(e1,e2,Annotation.create $loc)}
| t = type_basic id = ID {Variable_declaration(id,t,Annotation.create $loc)} // annotation
| IF e = expr i1 = block ELSE i2 = block {IfThenElse(e,i1,i2,Annotation.create $loc)}
| FOR name=ID FROM ei =expr TO et=expr STEP es=expr body=block {For(name,ei,et,es,body,Annotation.create $loc)}
| FOREACH name=ID IN l=expr body=block {Foreach(name,l,body,Annotation.create $loc)}
| GREY c=expr { let (red,green,blue)=(Field_accessor(Red_accessor,c,Annotation.create $loc),Field_accessor(Green_accessor,c,Annotation.create $loc),Field_accessor(Blue_accessor,c,Annotation.create $loc))
	in let sum1=Binary_operator(Add,red,green,Annotation.create $loc)
	in let sum2=Binary_operator(Add,sum1,blue,Annotation.create $loc)
	in let g=Binary_operator(Div,sum2,Constant_i(3,Annotation.create $loc),Annotation.create $loc)
	in let grey=Color(g,g,g,Annotation.create $loc)
	in Assignment(c,grey,Annotation.create $loc)}
| TRANSLATE p=expr OPENPA e1=expr COMMA e2=expr CLOSEPA {
	let (x,y)=(Field_accessor(X_accessor,p,Annotation.create $loc),Field_accessor(Y_accessor,p,Annotation.create $loc))
	in let (x2,y2)=(Binary_operator(Add,x,e1,Annotation.create $loc),Binary_operator(Add,y,e2,Annotation.create $loc))
	in Assignment(p,Point(x2,y2,Annotation.create $loc),Annotation.create $loc)
}
| {Nop}




type_basic:
| INT_TYP {Type_int}
| FLOAT_TYP {Type_float}
| BOOL_TYP {Type_bool}
| POS {Type_pos}
| COLOR {Type_color}
| POINT {Type_point}
| LIST OPENPA t=type_basic CLOSEPA {Type_list t}


argument:
| t = type_basic id = ID {Argument(id,t,Annotation.create $loc)} // annotation




expr:
| i = INT {Constant_i(i,Annotation.create $loc)}
| f = FLOAT {Constant_f(f,Annotation.create $loc)}
| b = BOOL {Constant_b(b,Annotation.create $loc)}
| COLOR OPENPA e1=expr COMMA e2=expr COMMA e3=expr CLOSEPA {Color(e1,e2,e3,Annotation.create $loc)}
| POINT OPENPA e1=expr COMMA e2=expr CLOSEPA {Point(e1,e2,Annotation.create $loc)}
| e1 = expr op = binop e2 = expr {Binary_operator(op,e1,e2,Annotation.create $loc)}
| v=var {v}
| op = unop e = expr {Unary_operator(op,e,Annotation.create $loc)}
| e= expr DOT field=accessor {Field_accessor(field,e,Annotation.create $loc)}
| e1 = expr PREPEND e2 = expr {Cons(e1,e2,Annotation.create $loc)}
| OPENPA e=expr CLOSEPA {e}
| OPENCR l=expr_list CLOSECR {List(l,Annotation.create $loc)}
| COLOR OPENPA i=INT CLOSEPA {Color( (*implicit cast from int to color*)
	Constant_i(i/65536,Annotation.create $loc),
	Constant_i((i mod 65536)/256,Annotation.create $loc),
	Constant_i(i mod 256,Annotation.create $loc),
	Annotation.create $loc)}
| PI {Constant_f(acos (-1.),Annotation.create $loc)}
| POS OPENPA e1=expr COMMA e2=expr CLOSEPA {Pos(e1,e2,Annotation.create $loc)}

var:
| name=ID {Variable(name,Annotation.create $loc)}


%inline binop:
| PLUS   { Add }
| MINUS   { Sub }
| TIMES   { Mul }
| DIVIDE   { Div }
| MODULO  { Mod }
| OR    { Or }
| AND   { And }
| EQUALS    { Eq }
| NOTEQUALS  { Ne }
| LESSTHAN    { Lt }
| LESSTHANEQUALS   { Le }
| GREATERTHAN    { Gt }
| GREATERTHANEQUALS   { Ge }

%inline unop:
| MINUS {USub}
| NOT   {Not}
| HEAD {Head}
| TAIL {Tail}
| FLOOR {Floor}
| FLOAT_OF_INT {Float_of_int}
| COS {Cos}
| SIN {Sin}


%inline accessor:
|COLOR {Color_accessor}
|POS {Position_accessor}
|X{X_accessor}
|Y {Y_accessor}
|RED {Red_accessor}
|BLUE {Blue_accessor}
|GREEN {Green_accessor}

